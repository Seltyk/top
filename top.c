#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Use getopt_long for GNU systems
#ifdef __GNUC__
#include <getopt.h>
#define getopt(x, y, z)	getopt_long((x), (y), (z), longopts, &longindex)
#endif

//Use getopt for other POSIX systems
#ifndef __GNUC__
#include <unistd.h>
#endif

#include "hashmap.h"
#include "top.h"

/* I'm all UTF-8 here */

PairValue* emojipair(char* emojiptr, char* valptr) {
	PairValue* out = malloc(sizeof(PairValue));
	out->key = emojiptr;
	out->value = valptr;
	return out;
}

char* encode(char instr[], HashMap* map) {
	char* outstr = calloc(36 * strlen(instr) + 1, sizeof(char));
	size_t inpos = 0;
	size_t outpos = 0;
	char byte;
	while(byte = instr[inpos++]) {
		for(size_t i = 0; i < map->length; i++) {
			PairValue* tmp = map->data[i];
			if(byte & *(char*) (tmp->value)) {
				for(size_t j = 0; j < 4; j++)
					outstr[outpos++] = ((char*) (tmp->key))[j];
			}
		}

		//byte separator
		outstr[outpos++] = 240;
		outstr[outpos++] = 159;
		outstr[outpos++] = 152;
		outstr[outpos++] = 136;
	}

	return realloc(outstr, outpos + 1);
}

//forgive me father for I am about to sin
char* decode(char instr[], HashMap* map) {
	char* outstr = calloc(strlen(instr)/4, sizeof(char));
	char* emoji = malloc(4 * sizeof(char));
	char byte = 0;
	size_t inpos = 0;
	size_t outpos = 0;
	while(inpos < strlen(instr)) {
		for(int i = 0; i < 4; i++) {
			emoji[i] = instr[inpos++];
		}

		//byte separator
		if(*(int*) emoji == -2003263504) {
			outstr[outpos++] = byte;
			byte = 0;
			continue;
		}

		for(size_t i = 0; i < map->length; i++) {
			PairValue* tmp = map->data[i];
			if(*(int*) emoji == *(int*) (tmp->key)) {
				byte |= *(char*) (tmp->value);
				break;
			}
		}
	}

	free(emoji);
	return realloc(outstr, outpos + 1);
}

int main(int argc, char* argv[]) {

	//create emoji
	char* smirk = malloc(4 * sizeof(char));
	smirk[0] = 240;
	smirk[1] = 159;
	smirk[2] = 152;
	smirk[3] = 143;
	char* okhand = malloc(4 * sizeof(char));
	okhand[0] = 240;
	okhand[1] = 159;
	okhand[2] = 145;
	okhand[3] = 140;
	char* sunglasses = malloc(4 * sizeof(char));
	sunglasses[0] = 240;
	sunglasses[1] = 159;
	sunglasses[2] = 152;
	sunglasses[3] = 142;
	char* eyebrow = malloc(4 * sizeof(char));
	eyebrow[0] = 240;
	eyebrow[1] = 159;
	eyebrow[2] = 164;
	eyebrow[3] = 168;
	char* metal = malloc(4 * sizeof(char));
	metal[0] = 240;
	metal[1] = 159;
	metal[2] = 164;
	metal[3] = 152;
	char* neutral = malloc(4 * sizeof(char));
	neutral[0] = 240;
	neutral[1] = 159;
	neutral[2] = 152;
	neutral[3] = 144;
	char* eggplant = malloc(4 * sizeof(char));
	eggplant[0] = 240;
	eggplant[1] = 159;
	eggplant[2] = 141;
	eggplant[3] = 134;
	char* adultsonly = malloc(4 * sizeof(char));
	adultsonly[0] = 240;
	adultsonly[1] = 159;
	adultsonly[2] = 148;
	adultsonly[3] = 158;

	//create values
	char* zero = malloc(sizeof(char));
	*zero = 1;
	char* one = malloc(sizeof(char));
	*one = 2;
	char* two = malloc(sizeof(char));
	*two = 4;
	char* three = malloc(sizeof(char));
	*three = 8;
	char* four = malloc(sizeof(char));
	*four = 16;
	char* five = malloc(sizeof(char));
	*five = 32;
	char* six = malloc(sizeof(char));
	*six = 64;
	char* seven = malloc(sizeof(char));
	*seven = 128;

	//create emoji -> bit hashmap
	HashMap* map = malloc(sizeof(HashMap));
	HashInit(map);
	HashAdd(map, emojipair(smirk, zero));
	HashAdd(map, emojipair(okhand, one));
	HashAdd(map, emojipair(sunglasses, two));
	HashAdd(map, emojipair(eyebrow, three));
	HashAdd(map, emojipair(metal, four));
	HashAdd(map, emojipair(neutral, five));
	HashAdd(map, emojipair(eggplant, six));
	HashAdd(map, emojipair(adultsonly, seven));

	//getopt and getopt_long options handling
	int opt;
	int d = 0;
	int longindex = 0;
	static struct option longopts[] = {
		/* NAME		TYPE			FLAG	SHORT */
		{"decode",	no_argument,	NULL,	'd'}
	};

	//read for decode mode
	while((opt = getopt(argc, argv, "d")) != -1) {
		if(opt == 'd') d = 1;
	}

	//rev up those friers
	if(optind < argc) {
		char* out = d ? decode(argv[optind], map) : encode(argv[optind], map);
		printf("%s\n", out);
		free(out);
	}

	//free the map and gtfo
	HashFree(map);
	free(map->data);
	free(map);
	return 0;
}
