# Top

A joke "forked from" [a joke](https://github.com/bottom-software-foundation/spec).  
Unlike that project, this repo provides the spec and reference implementations in Lua and C.

# Spec

## Implementation

Top can be implemented in many ways. Strictly speaking, all that's required is an encoder and a decoder that otherwise
matches the spec. The Lua reference acts as a library – users load it with `require "top"` and can then use the included
`encode` and `decode` functions. The C reference acts as a standalone program – it is intended to be used with stdin and
decodes iff the `-d` or `--decode` flag is provided. Because of the nature of C and the Unix pipeline, input and output
strings are NUL-terminated. Truth be told, the C implementation could also serve as a library.

## Encoding

### Input

Any Top implementation must accept exactly one input: the string to be encoded. The string is read from beginning to end
(excluding a NUL-terminator) and encoded completely.

The input string's format is irrelevant as Top interprets it as nothing more than an ordered list of bytes. The inputs
must not be altered; these are pure functions only, you commie OOP bastards!

### Output

Implementations must have one output, i.e. the string of emoji resulting from encoding. If you do not specify a text
format other than UTF-8, then output in UTF-8. The output string should be NUL-terminated in relevant situations, e.g. C
implementations intended to make use of the Unix pipeline. Optionally, a second output can be the number of emoji in the
output string.

## Decoding

### Input

Again, only one input is accepted: the string to be decoded. Again, the string is read from beginning to end (excluding
a NUL-terminator) and decoded completely.

The input string's format must match the format of your encoder's output. For example, the reference Lua implementation
encodes to and decodes from UTF-8.

### Output

Implementations have only one output, i.e. the result of decoding. You do not need to specify the format of this output
as the nature of Top will simply reconstruct the bytes exactly as they were first encoded. As you may have guessed, the
output should be NUL-terminated in relevant circumstance.

## Translation table

Each byte of the input string is turned into 0 to 8 emoji, each representing a bit of that byte, followed by a byte
separator. I know this is inefficient but I really don't care; this is a joke project.

| Unicode   | Character | Bit/value |
|-----------|-----------|-----------|
| `U+1f608` | 😈        | Byte sep. |
| `U+1f60f` | 😏        | 0/1       |
| `U+1f44c` | 👌        | 1/2       |
| `U+1f60e` | 😎        | 2/4       |
| `U+1f928` | 🤨        | 3/8       |
| `U+1f918` | 🤘        | 4/16      |
| `U+1f610` | 😐        | 5/32      |
| `U+1f346` | 🍆        | 6/64      |
| `U+1f51e` | 🔞        | 7/128     |

The order of the emoji between byte separators does not matter. So long as the sum of those emoji results in the byte
being encoded, and therefore is some integer between 0 and 255 inclusive, the encoded output is legal. Of course, the
output must be legal in your implementation's text formatting as well!
